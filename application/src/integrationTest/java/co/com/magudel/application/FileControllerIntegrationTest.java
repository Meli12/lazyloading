package co.com.magudel.application;

import co.com.magudel.model.mudanza.dto.WorkInput;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.restdocs.payload.RequestFieldsSnippet;
import org.springframework.restdocs.restassured3.RestDocumentationFilter;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static io.restassured.RestAssured.given;
import static org.hamcrest.core.Is.is;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.*;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields;
import static org.springframework.restdocs.restassured3.RestAssuredRestDocumentation.document;
import static org.springframework.restdocs.restassured3.RestAssuredRestDocumentation.documentationConfiguration;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = Main.class)
@ExtendWith({RestDocumentationExtension.class, SpringExtension.class})
public class FileControllerIntegrationTest {

    private RequestSpecification documentationSpec;

    @BeforeEach
    public void setUp(RestDocumentationContextProvider restDocumentation) {
        this.documentationSpec = new RequestSpecBuilder()
                .addFilter(documentationConfiguration(restDocumentation))
                .build();
    }

    @LocalServerPort
    private void initRestAssured(final int localPort) {
        RestAssured.port = localPort;
        RestAssured.baseURI = "http://localhost";

    }

    @Test
    public void executeEndpoint(){
        RequestFieldsSnippet requestFieldsSnippet = requestFields(
                fieldWithPath("contentBase64").description("file content in base64 format").type("String"),
                fieldWithPath("identification").description("The person identification that will be saved in DB")
                        .type("String")
        );

        RestDocumentationFilter docs = getSpecDoc("api-file", requestFieldsSnippet);

        WorkInput workInput = new WorkInput();
        workInput.setContentBase64("NQo0CjMwCjMwCjEKMQozCjIwCjIwCjIwCjExCjEKMgozCjQKNQo2CjcKOAo5CjEwCjExCjYKOQoxOQoyOQozOQo0OQo1OQoxMAozMgo1Ngo3Ngo4CjQ0CjYwCjQ3Cjg1CjcxCjkxCg==");
        workInput.setIdentification("123456789");

        given(documentationSpec)
                .filter(docs)
                .contentType(ContentType.JSON)
                .body(workInput)
                .when()
                .post("api/file")
                .then()
                .assertThat().statusCode(is(200));
    }

    protected RestDocumentationFilter getSpecDoc(String endpointName, RequestFieldsSnippet requestFieldsSnippet) {
        return document(endpointName, preprocessRequest(prettyPrint()), preprocessResponse(prettyPrint()), requestFieldsSnippet);
    }

}
