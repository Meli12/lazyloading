## LazyLoading

### Install Local
The application depends on Postgres. This item can be used with Docker.
```sh
docker run --name some-postgres -e POSTGRES_PASSWORD=mysecretpassword -d postgres
```

### Request sample with curl
```sh
$ curl 'http://localhost:55390/api/file' -i -X POST \
    -H 'Content-Type: application/json; charset=UTF-8' \
    -d '{
  "contentBase64" : "NQo0CjMwCjMwCjEKMQozCjIwCjIwCjIwCjExCjEKMgozCjQKNQo2CjcKOAo5CjEwCjExCjYKOQoxOQoyOQozOQo0OQo1OQoxMAozMgo1Ngo3Ngo4CjQ0CjYwCjQ3Cjg1CjcxCjkxCg==",
  "identification" : "123456789"
}'
``` 

#### HTTP response sample
```sh
HTTP/1.1 200 OK
Vary: Origin
Vary: Access-Control-Request-Method
Vary: Access-Control-Request-Headers
Content-Disposition: attachment; filename=report.txt
Content-Type: text/plain
Content-Length: 54

Case #1: 2
Case #2: 1
Case #3: 2
Case #4: 3
Case #5: 8
``` 

